module.exports = {
    extends: 'airbnb',
    rules: {
        'global-require': 0,
        'import/no-unresolved': 0,
        'no-param-reassign': 0,
        'no-shadow': 0,
        // allow debugger during development
        'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,
        'indent': ['error', 4],
        'no-confusing-arrow': ['error', {"allowParens": false}],
        'no-underscore-dangle': ['error', { "allowAfterThis": true }]
    }
}
