# eslint-config-rovergames

#### An ESLint [Shareable Config](http://eslint.org/docs/developer-guide/shareable-configs) for [personal uses](https://gitlab.com/users/rovergames/projects)

## Install

```bash
npm install --save-dev eslint-config-rovergames eslint
```

## Usage

Shareable configs are designed to work with the `extends` feature of `.eslintrc` files.
You can learn more about
[Shareable Configs](http://eslint.org/docs/developer-guide/shareable-configs) on the
official ESLint website.

To use the config, add this to your .eslintrc file:

```
{
  "extends": "rovergames"
}
```

*Note: I omitted the `eslint-config-` prefix since it is automatically assumed by ESLint.*

You can override settings from the shareable config by adding them directly into your
`.eslintrc` file.

## License

GPL-3.0

[My GitLab](https://gitlab.com/users/rovergames/projects)
